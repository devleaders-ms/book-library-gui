export const API_VERSION_PREFIX = "/v1/api";
export const API_SERVICE_URL = API_URL + API_VERSION_PREFIX;

export const BASIC_HEADERS = {
  'Content-Type': 'application/json',
  'Accept': 'application/json'
};

export const getBasicHeaders = () => (
  {
    'Content-Type': 'application/json',
    'Accept': 'application/json'
  }
);

export const getAuthHeaders = () => {
  let defaultHeaders = Object.assign({}, BASIC_HEADERS);
  defaultHeaders["Authorization"] ='Bearer ' + getAccessToken()
  return defaultHeaders
};

export const getAccessToken = () => localStorage.getItem('access_token');
export const getUserId = () => localStorage.getItem('user_id');
export const getReservationUrl = () => `/users/${getUserId()}/reservations`;
export const getRentalUrl = () => `/users/${getUserId()}/rentals`;

export const findEmptyInput = (obj) => {
  return Object.values(obj).includes('');
};
