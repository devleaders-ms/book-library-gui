import axios from 'axios';
import {
  API_SERVICE_URL,
  BASIC_HEADERS,
  getAccessToken,
  getAuthHeaders,
  getUserId
} from './apiUtils';
import {BehaviorSubject} from 'rxjs';
import decode from 'jwt-decode';

const LOGIN_URL = API_SERVICE_URL + '/auth/login';
const REGISTER_URL = API_SERVICE_URL + '/auth/register';
const EXCHANGE_TOKEN_URL = API_SERVICE_URL + '/auth/token';
export const UPDATE_USER_URL = `${API_SERVICE_URL}/users/${getUserId()}`;

axios.defaults.timeout = 10000;

const currentApiSubject = new BehaviorSubject(localStorage.getItem('access_token'));

export const currentUserData = {
  apiToken: currentApiSubject.asObservable()
};

export const loginRequest = (userLoginProps) => {
  let loginRequestPayload = JSON.stringify(userLoginProps);
  return axios
    .post(LOGIN_URL, loginRequestPayload, {
      headers: BASIC_HEADERS,
      withCredentials: true
    })
    .then(response => {
      if (response.status === 200) {
        return response.data
      }
      throw new Error(response.status)
    })
    .then(data => handleJwtTokenResponseData(data))
};

export const handleJwtTokenResponseData = (data) => {
  const decodedAccessToken = decode(data.accessToken);
  localStorage.setItem('access_token', data.accessToken);
  localStorage.setItem('user_name', decodedAccessToken.sub);
  localStorage.setItem('user_roles', decodedAccessToken.roles);
  localStorage.setItem('user_id', decodedAccessToken.userId);
  currentApiSubject.next(data.accessToken);
  return data;
};

export const registerRequest = (userRegisterProps) => {
  let registerRequestPayload = JSON.stringify(userRegisterProps);
  return axios
    .post(REGISTER_URL, registerRequestPayload, {
      headers: BASIC_HEADERS
    })
    .then(response => {
      if (response.status === 201) {
        return response
      }
      throw new Error(response.status)
    })
};

export const updateDataRequest = (userData) => {
  let updateRequestPayload = JSON.stringify(userData);
  return axios
    .patch(UPDATE_USER_URL, updateRequestPayload,{
      headers: getAuthHeaders()
    })
    .then(response => {
      if (response.status === 200) {
        return response;
      } else {
        throw new Error(response.status)
      }
    })
};

export const authRequiredRequest = (component, resourceAdress) => {
  return axios
    .get(API_SERVICE_URL + resourceAdress, {
      headers: getAuthHeaders()
    })
    .then(response => {
      if (response.status === 401) {
        component.props.history.push('/login');
        throw new Error(response.status)
      } else {
        return response
      }
    })
};

export const logout = () => {
  localStorage.removeItem('access_token');
  localStorage.removeItem('user_name');
  localStorage.removeItem('user_roles');
  localStorage.removeItem('user_email');
  localStorage.removeItem('user_id');
  currentApiSubject.next(null);
  history.push('/login')
};

axios.interceptors.response.use(
  response => response,
  async error => {
    const originalRequest = error.config;
    if (error.response.status === 401 && !originalRequest.url.endsWith("/login") && !originalRequest.url.endsWith("/token")) {
      try {
        const tokenResponse = await exchangeToken();
        handleJwtTokenResponseData(tokenResponse.data)
      } catch (e) {
        logout(this);
        return Promise.reject(error)
      }
      error.config.headers.Authorization = 'Bearer ' + getAccessToken()
      return axios(error.config);
    }
    return Promise.reject(error);
  }
);

const exchangeToken = async () => {
  return axios.post(
    EXCHANGE_TOKEN_URL,
    {},
    {
      headers: BASIC_HEADERS,
      withCredentials: true,
    },
  ).then(response => {
    if (response.status === 200) {
      return response;
    }
    return Promise.reject(new Error("Problem with token exchange"));
  })
};

export const statusExist = (err) => {
  return err.response !== undefined;
};
