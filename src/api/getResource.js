import axios from 'axios';
import {API_SERVICE_URL, BASIC_HEADERS} from './apiUtils';

axios.defaults.timeout = 10000;

export const getAsyncRes = (url, headers) => {
  return  axios
    .get(API_SERVICE_URL + url, {
      headers: headers()
    })
};
