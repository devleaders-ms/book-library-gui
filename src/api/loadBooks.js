import React, {useEffect, useState} from 'react';
import {getAsyncRes} from './getResource';
import {statusExist} from './authService';

const loadBooks = (url, headers) => {

  const [resources, setResources] = useState([]);
  const [isLoading, setLoading] = useState(true);
  const [isApiError, setApiError] = useState(false);
  const [isServerError, setServerError] = useState(false);
  const [isNotFound, setNotFound] = useState(false);
  const [isEmptyData, setEmptyData] = useState(false);

  const handleNotFound = (err) => {
    if (err.response.status === 404) {
      setLoading(false);
      setNotFound(true);
    }
  };

  const handleApiError = () => {
    setLoading(false);
    setApiError(true);
  };

  const handleServerError = err => {
    if (err.response.status === 500) {
      setLoading(false);
      setServerError(true);
    }
  };

  const handleCatch = (err) => {
    if (statusExist(err)) {
      handleNotFound(err);
      handleServerError(err);
    } else {
      handleApiError()
    }
  };

  const checkIfEmptyData = (data) => {
    if (!data.length) {
      setEmptyData(true);
    }
  };

  const loadBooks = async () => {
    try {
      const books = await getAsyncRes(url, headers);
      console.log(books.data);
      setResources(books.data);
      checkIfEmptyData(books.data)
      setLoading(false)
    } catch (err) {
      handleCatch(err);
    }
  };

  useEffect(() => {
    loadBooks()
  }, []);

  const errors = {
    isLoading,
    isServerError,
    isApiError,
    isNotFound,
    isEmptyData,
  };

  return ({errors, resources});
};

export default loadBooks;
