import axios from 'axios';
import {API_SERVICE_URL, getAuthHeaders} from './apiUtils';

export const changeUserRole = (component, userId, role) => {
  let requestPayload = JSON.stringify({"role" : role});
  return axios
    .patch(API_SERVICE_URL + "/users/" + userId + "/roles", requestPayload, {
      headers: getAuthHeaders()
    })
    .then(response => {
      if (response.status === 401) {
        component.props.history.push('/login');
        throw new Error(response.status)
      } else {
        return response
      }
    })
}
