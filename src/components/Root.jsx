import React, {useEffect, useState} from 'react';
import {BrowserRouter, Route, Switch} from 'react-router-dom';
import Header from './header/Header';
import GlobalStyle from '../theme/GlobalStyle';
import {currentUserData} from '../api/authService';
import {AuthenticationContext} from './context/context';
import decode from 'jwt-decode';
import ROLES from './Roles';
import MainPage from './main/MainPage';
import Books from './books/Books';
import {NotAuthenticatedRoutes} from './routes/NotAuthenticatedRoutes';
import {AuthenticatedRoutes} from './routes/AuthenticatedRoutes';
import InformationAfterUpdate
  from './informationAfterUpdate/InformationAfterUpdate';

const Root = () => {
  const [isAuthenticated, setIsAuthenticated] = useState(false);
  const [highestRole, setHighestRole] = useState(false);
  const [registerSuccess, setRegisterSuccess] = useState(false);
  const [user, setUser] = useState('');
  const [updateSuccess, setIsUpdateSuccess] = useState(false);

  useEffect(() => {
    currentUserData.apiToken.subscribe(token => {
      setIsAuthenticated(token);
      if (token) {
        const decodedToken = decode(token);
        console.log(decodedToken);
        const role = getHighestRole(decodedToken.roles);
        setHighestRole(role);
      }
    });
  }, []);

  useEffect(() => {
    setTimeout(() => {
        setRegisterSuccess(false);
        setIsUpdateSuccess(false);
      },
      5000)
  }, [updateSuccess, registerSuccess]);

  const permissions = {
    isAuthenticated,
    highestRole
  };

  const updateStatusData = {
    registerSuccess,
    updateSuccess,
    user,
  };

  const popupData = {
    setRegisterSuccess,
    setIsUpdateSuccess,
    setUser,
  };

  return (
    <BrowserRouter>
      <AuthenticationContext.Provider value={popupData}>
        <GlobalStyle/>
        <Header {...permissions}/>
        <div className='container mt-4 pl-4'>
          <Switch>
            <Route exact path='/' component={MainPage}/>
            <Route path='/books' component={Books}/>
            {
              isAuthenticated
                ? <AuthenticatedRoutes highestRole={highestRole}/>
                : <NotAuthenticatedRoutes/>
            }
          </Switch>
        </div>
        <InformationAfterUpdate {...updateStatusData}/>
      </AuthenticationContext.Provider>
    </BrowserRouter>
  );

  function getHighestRole(roles) {
    if (roles.includes(ROLES.ADMIN)) {
      return ROLES.ADMIN;
    } else if (roles.includes(ROLES.LIBRARIAN)) {
      return ROLES.LIBRARIAN;
    } else if (roles.includes(ROLES.USER)) {
      return ROLES.USER;
    } else {
      return ROLES.UNKNOWN;
    }
  }
};

export default Root
