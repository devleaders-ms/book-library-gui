import React from 'react';
import PropTypes from 'prop-types';

const TableHead = ({headersList}) => {
  return (
    <thead>
    <tr>
      {(headersList !== undefined) && (
        headersList.map(item => <th key={item}>{item}</th>)
      )}
    </tr>
    </thead>
  );
};

TableHead.propTypes = {
  headersList: PropTypes.array,
};

export default TableHead;
