import React, {useEffect, useState} from 'react';

const handleCheckbox = (data) => {
  const [dataWithCheckbox, setDataWithCheckbox] = useState([]);

  useEffect(() => {
    const checkbox = data.map(item => ({
        ...item,
        selected: false
      }
    ));
    setDataWithCheckbox(checkbox);
  }, []);


  const handleChange = (e) => {
    const {checked, id} = e.target;
    const update = dataWithCheckbox.map((item, i) =>
      i == id ? {...item, selected: checked} : item
    );

    setDataWithCheckbox(update);
  };

  const checked = () => (
    dataWithCheckbox.filter(item => item.selected)
  );

  const transferData = {
    dataWithCheckbox,
    handleChange,
    checked,
  };

  return (transferData);
};


export default handleCheckbox;
