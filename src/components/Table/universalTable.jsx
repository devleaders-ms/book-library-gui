import React, {useEffect, useState} from 'react';
import Table from 'react-bootstrap/Table';
import Form from 'react-bootstrap/Form';
import handleCheckbox from './handleCheckbox';
import TableHead from './TableHead';
import styled from 'styled-components';

const StyledTd = styled.td`
  width: 50px;
  text-align:center;
`;

const universalTable = (headersList, bodyData, filteredUser) => {

  const [filteredByUser, setFilteredByUser] = useState([]);

  const {dataWithCheckbox, handleChange, checked} = handleCheckbox(bodyData);

  useEffect(() => {
    setFilteredByUser(dataWithCheckbox)
  }, [dataWithCheckbox]);

  useEffect(() => {
    const filtered = dataWithCheckbox.filter(item => item.userId === filteredUser.firstName);
    setFilteredByUser(filtered);
  }, [filteredUser]);

  const getValues = (obj) => (
    Object.values(obj)
  );

  const generateRandomKey = () => (
    Math.random().toString(36).substring(7)
  );

  const checkIfNotLast = (obj, index) => (
    (getValues(obj).length - 1) > index
  );

  const table = () => (
    <Table striped bordered hover>
      <TableHead headersList={headersList}/>
      <tbody>
      {(filteredByUser !== undefined) && (
        filteredByUser.map((item, i) => (
          <tr key={item.id}>
            {getValues(item).map((val, j) => {
                if (checkIfNotLast(item, j)) {
                  return <td key={generateRandomKey()}>{val}</td>
                }
                return (
                  <StyledTd>
                    <Form.Check
                      checked={val}
                      type='checkbox'
                      id={i}
                      onChange={handleChange}
                    />
                  </StyledTd>
                )
              }
            )}
          </tr>
        ))
      )}
      </tbody>
    </Table>
  );

  return ({table, checked});
};

export default universalTable;
