import React, {useRef} from 'react';
import styled from 'styled-components';
import rgba from 'polished/lib/color/rgba';
import Button from 'react-bootstrap/Button';
import {closeByClickOutside} from '../closeByClickOutside/closeByClickOutside';

const StyledWrapper = styled.div.attrs({
  className: 'd-flex w-25 position-fixed flex-column justify-content-between'
})`
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);
  background-color: ${rgba('white', 0.98)};
  box-shadow: 0px 20px 20px -5px ${rgba('grey', 0.3)};
  border: 2px solid #ddd;
  z-index: 2;
  min-height: 150px;
`;

const AcceptWindow = ({close, children}) => {
  const ref = useRef(null);
  closeByClickOutside(close, ref);

  //todo
  const handleCancelReservation = () => {
    console.log('cancel')
  };

  return (
    <StyledWrapper ref={ref}>
      <div className='d-flex flex-grow-1 align-items-center ml-5 font-weight-bold'>
        {children}
      </div>
      <div className='d-flex justify-content-end m-3'>
        <Button onClick={handleCancelReservation} className='btn btn-success m-1'>
          Potwierdź
        </Button>
        <Button onClick={close} className='btn btn-danger m-1'>
          Anuluj
        </Button>
      </div>
    </StyledWrapper>
  );
};

export default AcceptWindow;
