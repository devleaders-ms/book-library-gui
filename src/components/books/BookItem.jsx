import React from 'react';
import styled from 'styled-components';
import {StyledLink} from '../common/StyledLink';
import Button from 'react-bootstrap/Button';

const BookDescription = styled.div.attrs({
  className: 'd-flex flex-column mx-4'
})`
  width: 65%;
`;

const BookItem = ({id, authors, title, description, category, image}) => {
  const allAuthorsAsText = getAllAuthorsAsText();
  return (
    <div className='d-flex mb-3 justify-content-between p-3'
         style={{backgroundColor: '#fce7a7'}}>
      <div className='d-flex'>
        <img className='align-self-center'
             height='200'
             width='150'
             alt='book image'
             src={image}
        />
      </div>
      <BookDescription>
        <h4 className='align-self-center font-weight-bold'>{title}</h4>
        <h5><b>Authors: {allAuthorsAsText}</b></h5>
        <div><b>Category:</b> {category}</div>
        <div><b>Description: </b><br/>{description.slice(0, 150)}...</div>
      </BookDescription>
      <div className='d-flex'>
        <StyledLink className='align-self-end' to={`/book/${id}`}>
          <Button>Czytaj więcej</Button>
        </StyledLink>
      </div>
    </div>
  );

  function getAllAuthorsAsText() {
    return authors.map(author => author.name + ' ' + author.lastName).join(', ');
  }

};

export default BookItem;
