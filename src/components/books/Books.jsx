import React from 'react';
import Categories from './Categories';
import BookItem from './BookItem';
import getBooksTheme from '../theme/getBooksTheme';
import {getBasicHeaders} from '../../api/apiUtils';
import LackBooks from '../errors/LackBooks';

const Books = () => {
  const {handleErrors, resources, isEmptyData} = getBooksTheme('/books', getBasicHeaders)

  return (
    <>
      {handleErrors()}
      {isEmptyData && <LackBooks/>}
      {resources.length && (
        <div className='d-flex justify-content-around mt-5'>
          <div className='w-25 pr-5'>
            <Categories/>
          </div>
          <div className='d-flex flex-column w-75'>
            {resources.map(book => <BookItem key={book.id} {...book}/>)}
          </div>
        </div>
      )}
    </>
  );
};

export default Books;
