import React from 'react';
import ListGroup from 'react-bootstrap/ListGroup'

const Menu = () => {
  return (
    <>
      <h3>Categories:</h3>
      <ListGroup>
        <ListGroup.Item action variant='secondary'>All Books</ListGroup.Item>
        <ListGroup.Item action variant='secondary'>Category1</ListGroup.Item>
        <ListGroup.Item action variant='secondary'>Category2</ListGroup.Item>
        <ListGroup.Item action variant='secondary'>Category3</ListGroup.Item>
        <ListGroup.Item action variant='secondary'>Category4</ListGroup.Item>
        <ListGroup.Item action variant='secondary'>Category5</ListGroup.Item>
        <ListGroup.Item action variant='secondary'>Category6</ListGroup.Item>
      </ListGroup>
    </>
  );
};

export default Menu;
