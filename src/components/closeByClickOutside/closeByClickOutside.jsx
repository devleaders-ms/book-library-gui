import React, {useEffect} from 'react';

export const closeByClickOutside = (toggleWindow, ref) => {

  const closeWindowByClickOutside = e => {
    if (!ref.current || ref.current.contains(e.target)) {
      return;
    }
    toggleWindow();
  }

  useEffect(() => {
    document.addEventListener('mousedown', closeWindowByClickOutside);
    return () => {
      document.removeEventListener('mousedown', closeWindowByClickOutside)
    }
  })
};
