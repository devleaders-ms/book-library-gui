import styled, {css} from 'styled-components';
import Form from 'react-bootstrap/Form';
import React from 'react';
import ValidateInput from '../ValidateInput/ValidateInput';
import PropTypes from 'prop-types'

const StyledFormControl = styled(Form.Control).attrs({
  className: 'mt-1'
})`
  &::placeholder {
  color: #ddd;
  }

  ${({unauthorized}) => (
  unauthorized && css`
    border: 2px solid #d00;
  `
)}`;

const Input = ({isNotEmptyInput, differentPass, differentValues, ...props}) => {
  const errorMessage = [
    {
      error: !isNotEmptyInput,
      message: 'Pole nie może być puste'
    },
    {
      error: differentPass,
      message: 'Hasła nie są jednakowe.'
    },
    {
      error: differentValues,
      message: 'Wpisane wartości się różnią.'
    },
  ];
  return (
    <>
      {(!isNotEmptyInput || differentPass || differentValues) ? (
        <>
          {errorMessage
            .filter(err => err.error)
            .map(err => <ValidateInput key={err.message} message={err.message} {...props}/>)}
        </>
      ) : (
        <StyledFormControl {...props}/>
      )}
    </>
  )
};

Input.propTypes = {
  differentPass: PropTypes.bool,
  differentValues: PropTypes.bool,
  unauthorized: PropTypes.bool,
};

Input.defaultProps = {
  differentPass: false,
  differentValues: false,
  unauthorized: false,
};

export default Input;
