import styled from 'styled-components';

export const LogoImage = styled.img.attrs({
  className: 'mx-3'
})`
  width: 50px;
  height: 50px;
`;
