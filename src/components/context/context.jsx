import * as React from 'react';

export const AuthenticationContext = React.createContext();
export const BooksDataContext = React.createContext();
