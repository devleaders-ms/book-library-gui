import React from 'react';
import styled from 'styled-components';

const StyledError = styled.div.attrs({
  className: 'w-50 mt-5'
})`
  background-image: url('src/images/school-4775493_640.png');
  background-repeat: no-repeat;
  background-size: cover;
  height: 400px;
`

const BooksNotFound = () => (
  <div className='d-flex justify-content-center align-items-center flex-column'>
    <h5 className='text-center text-danger mt-5'>
      Coś poszło nie tak, spróbuj ponownie za chwilę.
    </h5>
    <StyledError/>
  </div>
  );

export default BooksNotFound;
