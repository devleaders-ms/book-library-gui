import React from 'react';
import {StyledLink} from '../common/StyledLink';

const LackOfBooksMessage = ({libraryOption}) => (
    <h2 className='text-center'>
      <div>Nie posiadasz jeszcze żadnych książek!</div>
      <div>Przejdź do sekcji <span className='text-primary'><StyledLink to='/books'>Książki</StyledLink></span> żeby {libraryOption}.</div>
    </h2>
  );

export default LackOfBooksMessage;
