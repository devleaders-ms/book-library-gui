import React from 'react';
import PropTypes from 'prop-types';

const UnauthorisedAlert = ({secondary, children}) => {
  return (
    <>
        {secondary ? (
          <div className="alert alert-danger" role="alert">
            {children}
          </div>
        ) : (
          < div className="alert alert-success" role="alert">
            {children}
          </div>
        )}
    </>
  );
};

UnauthorisedAlert.propTypes = {
  condition: PropTypes.bool,
};

export default UnauthorisedAlert;
