import React, {useEffect, useRef, useState} from 'react';

export const focusBook = (focusBookRef) => {
  const [focusedBook, setFocusedBook] = useState(false);

  const handlerFocusBook = e => {
    if (!focusBookRef.current || focusBookRef.current.contains(e.target)) {
      setFocusedBook(true)
    } else {
      setFocusedBook(false)
    }
  }

  useEffect( () => {
    document.addEventListener('mouseover', handlerFocusBook)
    return () => {
      document.removeEventListener('mouseover', handlerFocusBook)
    }
  });

  return (focusedBook);
};
