import {Link} from 'react-router-dom';
import React from 'react';
import Button from 'react-bootstrap/Button';


export const AuthenticatedHeaderInfo = () => {
  return (
    <>
      <Link className='align-self-end' to={'/my-profile'}>
        <Button className='rounded-circle'>
          {localStorage.getItem('user_name').charAt(0).toUpperCase()}
        </Button>
      </Link>
    </>
  );
};
