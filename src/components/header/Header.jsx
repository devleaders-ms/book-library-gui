import React from 'react';
import styled from 'styled-components';
import {StyledLink} from '../common/StyledLink'
import ROLES from '../Roles';
import {AuthenticatedHeaderInfo} from './AuthenticatedHeaderInfo';
import {UnauthenticatedHeaderInfo} from './UnauthenticatedHeaderInfo';
import {GuestHeaderTabs} from './tabs/GuestHeaderTabs';
import {AdminHeaderTabs} from './tabs/AdminHeaderTabs';
import {UserHeaderTabs} from './tabs/UserHeaderTabs';
import {LibrarianHeaderTabs} from './tabs/LibrarianHeaderTabs';

const logoIcon = require('../../images/logo.png');

const Header = ({isAuthenticated, highestRole}) => {

  const headerInfoComponent = getHeaderInfoComponent();
  const headerTabs = getHeaderTabs();

  return (
    <div className='d-flex border-bottom'>
      <div className='container'>
        <div className='d-flex justify-content-between'>
          <StyledLink to="/">
            <div className='d-flex flex-row align-items-center'>
              <LogoImage src={logoIcon}/>
              <h4 className='font-weight-bold'>Wypożyczalnia książek</h4>
            </div>
          </StyledLink>
          <div className='d-flex flex-column justify-content-center w-25'>
            {headerInfoComponent}
          </div>
        </div>
        <div className='d-flex flex-row justify-content-between'>
          {headerTabs}
        </div>
      </div>
    </div>
  );

  function getHeaderInfoComponent() {
    return isAuthenticated
      ? <AuthenticatedHeaderInfo/>
      : <UnauthenticatedHeaderInfo/>;
  }

  function getHeaderTabs() {
    if (!isAuthenticated) {
      return <GuestHeaderTabs/>
    }
    switch (highestRole) {
      case ROLES.ADMIN:
        return <AdminHeaderTabs/>
      case ROLES.LIBRARIAN:
        return <LibrarianHeaderTabs/>
      case ROLES.USER:
        return <UserHeaderTabs/>
      default:
        return <GuestHeaderTabs/>
    }
  }

}

export default Header;

const LogoImage = styled.img`
  width: 100px;
  height: 100px;
  margin: 15px;
`;
