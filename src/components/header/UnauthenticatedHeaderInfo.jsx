import {Link} from 'react-router-dom';
import React from 'react';
import Button from 'react-bootstrap/Button';

export const UnauthenticatedHeaderInfo = () => {
  return (
    <>
      <Link to='/login'>
        <Button className='my-1 w-100'
                variant='outline-dark'>
          Zaloguj
        </Button>
      </Link>
      <Link to='/register'>
        <Button className='my-1 w-100'
                variant='outline-dark'>
          Zarejestruj
        </Button>
      </Link>
    </>
  );
};
