import HeaderTab from './HeaderTab';
import React from 'react';
import {StyledLink} from '../../common/StyledLink';

export const AdminHeaderTabs = () => {
  return (
    <>
      <HeaderTab>
        <StyledLink to='/manage-users'>Zarządzaj użytkownikami</StyledLink>
      </HeaderTab>
      <HeaderTab>Zarządzaj stawkami</HeaderTab>
      <HeaderTab>Statystyki</HeaderTab>
    </>
  )
}
