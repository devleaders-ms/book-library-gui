import React from 'react';
import HeaderTab from './HeaderTab';
import {StyledLink} from '../../common/StyledLink';

export const GuestHeaderTabs = () => {
  return (
    <>
      <HeaderTab><StyledLink to='/books'>Książki</StyledLink></HeaderTab>
      <HeaderTab>Top 20!</HeaderTab>
      <HeaderTab>Kontakt</HeaderTab>
    </>
  )
}
