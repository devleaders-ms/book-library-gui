import styled from 'styled-components';

const HeaderTab = styled.div.attrs({
  className: 'h5 border text-center flex-grow-1 mb-0 p-1'
})`
  &:hover {
    font-weight:bold;
  }
`;

export default HeaderTab;
