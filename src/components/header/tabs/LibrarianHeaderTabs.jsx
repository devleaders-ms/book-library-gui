import HeaderTab from './HeaderTab';
import React from 'react';
import {StyledLink} from '../../common/StyledLink';

export const LibrarianHeaderTabs = () => {
  return (
    <>
      <HeaderTab>Zarządzaj książkami</HeaderTab>
      <HeaderTab>Wypożycz/Oddaj</HeaderTab>
      <HeaderTab>
        <StyledLink to='/manageReservations'>
          Zarządzaj rezerwacjami
        </StyledLink>
      </HeaderTab>
    </>
  )
};
