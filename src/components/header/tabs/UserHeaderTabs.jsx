import HeaderTab from './HeaderTab';
import React from 'react';
import {StyledLink} from '../../common/StyledLink';

export const UserHeaderTabs = () => {
  return (
    <>
      <HeaderTab>
        <StyledLink to='/books'>Książki</StyledLink>
      </HeaderTab>
      <HeaderTab>
        <StyledLink to='/my-rentals'>Wypożyczone</StyledLink>
      </HeaderTab>
      <HeaderTab>
        <StyledLink to='/my-reservation'>Rezerwacje</StyledLink>
      </HeaderTab>
      <HeaderTab>Top 20!</HeaderTab>
      <HeaderTab>Kontakt</HeaderTab>
    </>
  )
}
