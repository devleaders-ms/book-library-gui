import React from 'react';
import ModalWindow from '../modal/ModalWindow';
import PropTypes from 'prop-types';

const InformationAfterUpdate = ({registerSuccess, updateSuccess, user}) => {
  return (
    <>
      {registerSuccess && (
        <ModalWindow activeButton buttonName='Zaloguj'>
          {`Gratulacje, zarejestrowałeś się jako ${user}. Zaloguj się aby sięgnąć po więcej.`}
        </ModalWindow>)}
      {updateSuccess && (
        <ModalWindow>
          Gratulacje! Twoje dane zostały zaktualizowane.
        </ModalWindow>
      )}
    </>
  );
};

InformationAfterUpdate.propTypes = {
  registerSuccess: PropTypes.bool,
  updateSuccess: PropTypes.bool,
  user: PropTypes.string,
};

InformationAfterUpdate.defaultProps = {
  registerSuccess: false,
  updateSuccess: false,
  user: '',
};

export default InformationAfterUpdate;
