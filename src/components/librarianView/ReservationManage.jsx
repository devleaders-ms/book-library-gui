import React from 'react';
import getBooksTheme from '../theme/getBooksTheme';
import {getAuthHeaders} from '../../api/apiUtils';
import LackBooks from '../errors/LackBooks';
import ManageReservationTable from '../tables/ManageReservationTable';
import {tableData, tableHead} from '../../exampleData/exampleData';

const ReservationManage = props => {

  const {handleErrors, resources, isEmptyData} = getBooksTheme('/reservations', getAuthHeaders);

  return (
    <>
      {handleErrors()}
      {isEmptyData && <LackBooks/>}
      {/*TO DO*/}
      {/*{resources.length &&*/}
      {tableData.length &&
      <ManageReservationTable headersList={tableHead} bodyData={tableData}/>
      }
    </>
  );
};

export default ReservationManage;
