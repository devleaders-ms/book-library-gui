import React from 'react';

const LoadingButton = () => {
  return (
    <div  className='mt-4'>
      <button className="btn btn-primary" type="button"  disabled>
        <span className="spinner-border spinner-border-sm" role="status"
              aria-hidden="true"></span>
        Wczytywanie...
      </button>
    </div>
  );
};

export default LoadingButton;
