import React from 'react';
import Loader from 'react-loader-spinner';
import styled, {css} from 'styled-components';
import rgba from 'polished/lib/color/rgba';

const StyledWrapper = styled.div.attrs({
  className: 'd-flex justify-content-center'
})`
  position: absolute;
`;

const StyledCenteringWrapper = styled.div`
  ${({center}) => (
  center && css`
     display: flex;
     justify-content: space-around;
  `
)}
`;

const StyledLoader = styled(Loader).attrs({
  className: 'p-5'
})`
  background-color: ${rgba('white', 0.7)};
`;

const LoadingSpinner = ({center}) => (
  <StyledCenteringWrapper center={center}>
  <StyledWrapper>
    <StyledLoader
      type='Oval'
      color='#0066ff'
      height={150}
      width={150}
    />
  </StyledWrapper>
  </StyledCenteringWrapper>
);


export default LoadingSpinner;
