import React, {useEffect, useReducer, useState} from 'react';
import LoginInput from './LoginInput';
import {loginRequest, statusExist} from '../../api/authService';
import {findEmptyInput} from '../../api/apiUtils';
import {useHistory} from 'react-router';

const Login = () => {
  const [credential, setCredential] = useReducer((state, prevState) => ({...state, ...prevState}),
    {
      login: '',
      password: '',
    });

  const [notEmptyInputs, setNotEmptyInputs] = useReducer((state, prevState) => ({...state, ...prevState}),
    {
      login: true,
      password: true,
    });

  const [unauthorized, setUnauthorized] = useState(false);
  const [isLoading, setIsLoading] = useState(false);
  const [isApiError, setApiError] = useState(false);
  const [isServerError, setServerError] = useState(false);

  useEffect(() => {
    setTimeout(() => {
      setServerError(false);
      setApiError(false);
    }, 8000)
  }, [isServerError, isApiError]);

  const isEmptyInputs = findEmptyInput(credential);
  const history = useHistory();

  const handleChange = (e) => {
    const {id, value} = e.target;
    setCredential({
      [id]: value.trim()
    })
  };

  const resetIsEmptyInputs = () => {
    setNotEmptyInputs({
      login: true,
      password: true,
    })
  };

  const handleServerError = err => {
    if (err.response.status === 500) {
      setIsLoading(false);
      setServerError(true);
    }
  };

  const handleUnauthorised = err => {
    if (err.response.status === 401) {
      setUnauthorized(true);
      setIsLoading(false);
      setCredential({
        login: '',
        password: '',
      });
    }
  };

  const handleApiError = () => {
    setIsLoading(false);
    setApiError(true);
  };

  const handleCatch = (err) => {
    if (statusExist(err)) {
      handleServerError(err);
      handleUnauthorised(err)
    } else {
      handleApiError(err)
    }
  };

  const handleOnSubmit = (e) => {
    e.preventDefault();
    setIsLoading(true);
    resetIsEmptyInputs();

    if (!isEmptyInputs) {
      loginRequest(credential)
        .then(() => {
          history.push('/');
        })
        .catch(err => {
          handleCatch(err)
        });
    } else {
      setIsLoading(false)
      setNotEmptyInputs({
        ...credential
      })
    }
  };

  const validData = {
    unauthorized,
    notEmptyInputs,
  };

  const themeData = {
    unauthorized,
    isLoading,
    isApiError,
    isServerError,
    onSubmit: handleOnSubmit,
  };

  const loginData = {
    credential,
    validData,
    themeData,
    onChange: handleChange,
  };
  return (
    <LoginInput {...loginData}/>
  );
};

export default Login;
