import React from 'react';
import PropTypes from 'prop-types';
import LoginRegisterTheme from '../theme/LoginRegisterTheme';
import Input from '../common/Input';
import PopUpAlert from '../popUpAlert/PopUpAlert';

const LoginInput = ({credential, validData, themeData, onChange}) => {

  return (
    <>
      <LoginRegisterTheme
        buttonName='Zaloguj'
        headerWindow='Zaloguj w'
        message='Nie możemy Cię znaleźć'
        {...themeData}
      >
        <Input
          id='login'
          value={credential.login}
          type='text'
          placeholder='login'
          onChange={onChange}
          unauthorized={validData.unauthorized}
          isNotEmptyInput={validData.notEmptyInputs.login}
        />
        <Input
          id='password'
          value={credential.password}
          type='password'
          placeholder='hasło'
          unauthorized={validData.unauthorized}
          onChange={onChange}
          isNotEmptyInput={validData.notEmptyInputs.password}
        />
      </LoginRegisterTheme>
    </>
  );
};

LoginInput.propTypes = {
  login: PropTypes.string,
  password: PropTypes.string,
  onChange: PropTypes.func,
  onClick: PropTypes.func,
  isLoading: PropTypes.bool,
  unauthorized: PropTypes.bool,
};

LoginInput.defaultProps = {
  login: '',
  password: '',
  unauthorized: false,
  isLoading: false,
};

export default LoginInput;
