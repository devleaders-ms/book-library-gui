import React, {useState} from 'react';
import {UserSelect} from './UsersSelect';
import {RoleSelect} from './RoleSelect';
import {changeUserRole} from '../../api/usersService';
import Button from 'react-bootstrap/Button';
import {ManageUsersToast} from './ManageUsersToast';

export const ManageUsers = () => {
  const [selectedUser, setSelectedUser] = useState(undefined);
  const [selectedRole, setSelectedRole] = useState(undefined);
  const [showToast, setShowToast] = useState(false);
  const [isError, setIsError] = useState(undefined);

  function handleUserChange(selectedUser) {
    setSelectedUser(selectedUser);
  }

  function handleRoleChange(selectedRole) {
    setSelectedRole(selectedRole);
  }

  function changeRole() {
    changeUserRole(this, selectedUser.id, selectedRole.value)
      .then(() => {
        setIsError(false)
        setShowToast(true)
      })
      .catch(() => {
        setIsError(true)
        setShowToast(true)
      })
  }

  return (
    <div className='d-flex flex-column align-items-center mt-5'>
      <ManageUsersToast isVisible={showToast} setShow={setShowToast} isError={isError}/>
      <UserSelect selectTitle='Wyszukaj użytkownika, aby zmienić uprawnienia:' handleChange={handleUserChange}/>
      {
        selectedUser &&
        <RoleSelect handleChange={handleRoleChange}/>
      }
      {
        selectedRole &&
        <Button onClick={() => changeRole()}>
          Modyfikuj role użytkownika
        </Button>
      }
    </div>
  )
};
