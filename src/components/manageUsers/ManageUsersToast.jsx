import {Toast} from 'react-bootstrap';
import React from 'react';
import styled from 'styled-components';

export const ManageUsersToast = ({isVisible, setShow, isError}) => {
  return (
    <>
      {!isError && <SuccessToast isVisible={isVisible} setShow={setShow}/>}
      {isError && <ErrorToast isVisible={isVisible} setShow={setShow}/>}
    </>
  )
}

const SuccessToast = ({isVisible, setShow}) => {
  return (
    <Toast style={{background: "rgba(157,255,30,0.79)"}}
           onClose={() => setShow(false)}
           show={isVisible}
           delay={3000}
           autohide>
      <Toast.Body>Zmiana roli użytkownika przebiegła pomyślnie</Toast.Body>
    </Toast>
  )
}

const ErrorToast = ({isVisible, setShow}) => {
  return (
    <Toast style={{background: "rgba(255,0,0,0.67)"}}
           onClose={() => setShow(false)}
           show={isVisible}
           delay={3000}
           autohide>
      <Toast.Body>Ups... Nie udało się zmienić roli użytkownika :(</Toast.Body>
    </Toast>
  )
}
