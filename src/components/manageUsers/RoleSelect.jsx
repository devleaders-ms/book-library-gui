import React from 'react';
import Select from 'react-select';
import ROLES from '../Roles';

export const RoleSelect = ({handleChange}) => {
  const roles = [
    {value: ROLES.ADMIN, label: "Admin"},
    {value: ROLES.LIBRARIAN, label: "Bibliotekarz"},
    {value: ROLES.USER, label: "Użytkownik"}
  ];
  return (
    <div className='w-50 my-4'>
      <div>Wybierz dostępna rolę do zmiany:</div>
      <Select options={roles}
              onChange={handleChange}
              placeholder={"Wybierz rolę..."}/>
    </div>
  )
}
