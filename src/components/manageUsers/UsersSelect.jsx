import {authRequiredRequest} from '../../api/authService';
import AsyncSelect from 'react-select/async/dist/react-select.esm';
import React from 'react';
import _ from 'lodash';

const loadUsersOptions = (input, callback) => {
  authRequiredRequest(this, "/users?fullName=" + input)
    .then(res => res.data)
    .then(data => callback(data))
};

const debouncedLoadOptions = _.debounce(loadUsersOptions, 500);

export const UserSelect = ({handleChange, selectTitle}) => {
  return (
    <div className='w-50 my-4'>
      <div>{selectTitle}</div>
      <div className='mt-2'>
        <AsyncSelect loadOptions={debouncedLoadOptions}
                     getOptionValue={option => option.id}
                     getOptionLabel={option => option.firstName + " " + option.lastName}
                     onChange={handleChange}
                     placeholder={"Imię lub nazwisko..."}
        />
      </div>
    </div>
  )
};
