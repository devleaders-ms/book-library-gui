import React, {useState} from 'react';
import PropTypes from 'prop-types';
import Modal from 'react-bootstrap/Modal';
import Button from 'react-bootstrap/Button';
import {StyledLink} from '../common/StyledLink';

const ModalWindow = ({buttonName, activeButton, children}) => {
  const [show, setShow] = useState(true);

  const handleClose = () => setShow(false);
  return (
    <>
      <Modal show={show} onHide={handleClose}>
        <Modal.Body>{children}</Modal.Body>
        <Modal.Footer>
          <StyledLink to='/login'>
            {activeButton && (
              <Button variant="primary" onClick={handleClose}>
                {buttonName}
              </Button>)}
          </StyledLink>
        </Modal.Footer>
      </Modal>
    </>
  );
};

ModalWindow.propTypes = {
  buttonName: PropTypes.string.isRequired,
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node
  ]).isRequired,
};

export default ModalWindow;
