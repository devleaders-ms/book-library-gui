import React from 'react';
import styled from 'styled-components';
import RentalBook from './RentalBook';
import rentalsReservationTheme from '../theme/rentalsReservationTheme';
import {getRentalUrl} from '../../api/apiUtils';

const StyledGridTemplate = styled.div.attrs({
  className: 'w-75 mr-auto ml-auto'
})`
  display: grid;
  grid-template-columns: repeat(3, 1fr);
  justify-items: center;
  row-gap: 70px;
`;

const MyRentals = () => {
  const {rrTheme, resources} = rentalsReservationTheme(getRentalUrl());

  return (
    <>
      {rrTheme('Wypożyczone książki', 'wypożyczyć')}
      {resources.length && (
        <StyledGridTemplate>
          {resources.map(book => <RentalBook key={book.id} {...book}/>)}
        </StyledGridTemplate>
      )}
    </>
  );
};

export default MyRentals;
