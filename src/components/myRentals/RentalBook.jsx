import React, {useRef, useState} from 'react';
import styled from 'styled-components';
import rgba from 'polished/lib/color/rgba';
import {focusBook} from '../focusBook/focusBook';
import RentalBookDetails from './RentalBookDetails';
import PropTypes from 'prop-types'

const StyledWrapper = styled.div`
  position: relative;
  width: calc(72px * 2);
  height: calc(110px * 2);
`;

const StyledImage = styled.img`
  position: absolute;
  width: calc(72px * 2);
  height: calc(110px * 2);
`;

const StyledViewAfterFocus = styled.div.attrs({
  className: 'd-flex align-items-center'
})`
  position: absolute;
  width: calc(72px * 2);
  height: calc(110px * 2);
  background-color: ${rgba('grey', 0.8)};
  color: white;
  z-index: 2;
  text-align: center;
  letter-spacing: 1px;
`;

const RentalBook = ({img, ...props}) => {
  const [openDetails, setOpenDetails] = useState(false)
  const ref = useRef(null)
  const focus = focusBook(ref)

  const handleClick = () => {
    setOpenDetails(!openDetails)
  }

  const dataToDetailWindow = {
    ...props,
    img,
    close: () => handleClick()
  }

  return (
    <>
      <StyledWrapper ref={ref}>
        <StyledImage src={img}/>
        {focus && (
          <StyledViewAfterFocus onClick={handleClick}>
            Kliknij żeby wyświetlić szczegóły wypożyczenia
          </StyledViewAfterFocus>
        )}
      </StyledWrapper>
      {openDetails && <RentalBookDetails {...dataToDetailWindow}/>}
    </>
  );
};

RentalBook.propTypes = {
  props: PropTypes.object,
  img: PropTypes.string
}

export default RentalBook;
