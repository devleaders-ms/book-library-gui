import React, {useRef} from 'react';
import styled from 'styled-components';
import rgba from 'polished/lib/color/rgba';
import {closeByClickOutside} from '../closeByClickOutside/closeByClickOutside';
import PropTypes from 'prop-types'

const StyledWrapper = styled.div.attrs({
  className: 'w-50 position-fixed'
})`
  display: grid;
  grid-template-columns: 1fr 2fr;
  left: 50%;
  top: 50%;
  transform: translate(-50%, -50%);
  background-color: ${rgba('white', 0.98)};
  box-shadow: 0px 20px 40px -5px ${rgba('grey', 0.3)};
  border: 2px solid #ddd;
  z-index: 3;
  height: 300px;
`;

const StyledImage = styled.img`
  width: calc(72px * 2);
  height: calc(110px * 2);
  place-self: center;
  box-shadow: 0px 20px 40px -5px ${rgba('grey', 0.75)};
`;

const StyledInformation = styled.div`
  height: 200px;
  display: grid;
  align-items: center;
  grid-template-columns: 1fr 2fr;
  grid-template-rows: repeat(5, 1fr);
  grid-auto-flow: column;
`;

const StyledBold = styled.span.attrs({
  className: 'font-weight-bold'
})`
`;

const RentalBookDetails = ({bookName, copyNumber, rentalData, returnData, status, img, close}) => {
  const ref = useRef(null)
  closeByClickOutside(close, ref)

  return (
    <StyledWrapper ref={ref}>
      <StyledImage src={img}/>
      <div className='d-flex justify-content-center flex-column'>
        <StyledInformation>
          <div>Tytuł:</div>
          <div>Numer egzemplarza:</div>
          <div>Data wypożyczenia:</div>
          <div>Data oddania:</div>
          <div>Status:</div>
          <div><StyledBold>{bookName}</StyledBold></div>
          <div><StyledBold>{copyNumber}</StyledBold></div>
          <div><StyledBold>{rentalData}</StyledBold></div>
          <div><StyledBold>{returnData}</StyledBold></div>
          <div><StyledBold>{status}</StyledBold></div>
        </StyledInformation>
      </div>
    </StyledWrapper>
  );
};

RentalBookDetails.propTypes = {
  bookName: PropTypes.string.isRequired,
  copyNumber: PropTypes.number.isRequired,
  rentalData: PropTypes.string.isRequired,
  returnData: PropTypes.string.isRequired,
  status: PropTypes.string.isRequired,
  img: PropTypes.string,
  close: PropTypes.func
};

export default RentalBookDetails;
