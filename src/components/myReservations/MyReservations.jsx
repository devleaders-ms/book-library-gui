import React from 'react';
import ReservationBook from './ReservationBook';
import rentalsReservationTheme from '../theme/rentalsReservationTheme';
import {getReservationUrl} from '../../api/apiUtils';

const MyReservations = () => {

  const {rrTheme, resources} = rentalsReservationTheme(getReservationUrl());

  return (
    <>
      {rrTheme('Zarezerwowane Książki', 'zarezerwować')}
      {resources.length && (
        resources.map(book => <ReservationBook key={book.id} {...book}/>
        )
      )}
    </>
  )
};

export default MyReservations;
