import React, {useState} from 'react';
import styled from 'styled-components';
import rgba from 'polished/lib/color/rgba';
import {Button} from 'react-bootstrap';
import AcceptWindow from '../acceptWindow/AcceptWindow';
import PropTypes from 'prop-types';

const StyledWrapper = styled.div.attrs({
  className: 'w-75 mt-5 ml-auto mr-auto'
})`
  display: grid;
  grid-template-columns: 1fr 2fr;
  align-items: center;
  background-color: ${rgba('white', 1)};
  box-shadow: 0px 20px 40px -5px ${rgba('grey', 0.3)};
`;

const StyledImg = styled.img`
  justify-self: center;
  width: calc(72px * 2);
  height: calc(110px * 2);
`;

const StyledInformation = styled.div.attrs({
  className: 'mt-3 mr-3'
})`
  height: 200px;
  display: grid;
  align-items: center;
  grid-template-columns: 1fr 2fr;
  grid-template-rows: repeat(4, 1fr);
  grid-auto-flow: column;
`;

const StyledBold = styled.span.attrs({
  className: 'font-weight-bold'
})`
`;

const ReservationBook = ({bookName, copyNumber, reservationData, reservationOffData, img}) => {

  const [isOpen, setIsOpen] = useState(false);

  const handleClick = () => {
    setIsOpen(!isOpen);
  };

  return (
    <>
      <StyledWrapper>
        <StyledImg src={img}/>
        <div className='d-flex justify-content-center flex-column'>
          <StyledInformation>
            <div>Tytuł:</div>
            <div>Numer egzemplarza:</div>
            <div>Data rezerwacji:</div>
            <div>Data wygaśnięcia:</div>
            <div><StyledBold>{bookName}</StyledBold></div>
            <div><StyledBold>{copyNumber}</StyledBold></div>
            <div><StyledBold>{reservationData}</StyledBold>
            </div>
            <div><StyledBold>{reservationOffData}</StyledBold>
            </div>
          </StyledInformation>
          <Button
            onClick={handleClick}
            className='w-50 btn btn-danger align-self-end m-3'
          >
            Anuluj Rezerwację
          </Button>
        </div>
      </StyledWrapper>
      {isOpen && (
        <AcceptWindow close={handleClick}>
          Na pewno chcesz anulować rezerwację?
        </AcceptWindow>
      )}
    </>
  )
};

ReservationBook.propTypes = {
  bookName: PropTypes.string.isRequired,
  copyNumber: PropTypes.number.isRequired,
  reservationData: PropTypes.string.isRequired,
  reservationOffData: PropTypes.string.isRequired,
  img: PropTypes.string.isRequired,
};

export default ReservationBook;
