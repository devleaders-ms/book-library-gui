import React from 'react';
import PropTypes from 'prop-types';
import styled, {css} from 'styled-components';

const StyledWrapper = styled.div`
 ${({popup}) => (
    popup && css`
      position: fixed;
      top: 1%;
      right: 1%;
    `
  )}
`;

const PopUpAlert = ({popup, secondary, children}) => {

  return (
    <StyledWrapper popup={popup}>
      {secondary ? (
        <div className="alert alert-danger" role="alert">
          {children}
        </div>
      ) : (
        < div className="alert alert-success" role="alert">
          {children}
        </div>
      )}
    </StyledWrapper>
  )
};

PopUpAlert.propTypes = {
  condition: PropTypes.bool,
  popup: PropTypes.bool,
};

export default PopUpAlert;
