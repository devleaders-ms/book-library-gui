import React, {useContext, useEffect, useReducer, useState} from 'react';
import RegisterInput from './RegisterInput';
import {registerRequest, statusExist} from '../../api/authService';
import {AuthenticationContext} from '../context/context';
import {findEmptyInput} from '../../api/apiUtils';
import {useHistory} from 'react-router';

const Register = () => {
  const [credential, setCredential] = useReducer((state, prevState) => ({...state, ...prevState}),
    {
      login: '',
      email: '',
      password: '',
      repPass: '',
      firstName: '',
      lastName: '',
    });

  const [notEmptyInputs, setNotEmptyInputs] = useReducer((state, prevState) => ({...state, ...prevState}),
    {
      login: true,
      email: true,
      password: true,
      repPass: true,
      firstName: true,
      lastName: true,
    });

  const [isLoading, setIsLoading] = useState(false);
  const [differentPass, setIsDifferentPass] = useState(false);
  const [loginExist, setIsLoginExist] = useState(false);
  const [isApiError, setApiError] = useState(false);
  const [isServerError, setServerError]  = useState(false);

  const context = useContext(AuthenticationContext);
  const history = useHistory();


  useEffect(() => {
    setTimeout(() => {
      setServerError(false);
      setApiError(false);
    }, 8000)
  }, [isServerError, isApiError]);

  const isEmptyInputs = findEmptyInput(credential);

  const resetState = () => {
    setIsDifferentPass(false);
    setIsLoginExist(false)
    setIsLoading(true);
    setApiError(false);
  };

  const handleChange = (e) => {
    const {id, value} = e.target;
    setCredential({
      [id]: value.trim()
    })
  };

  const handleExistUser = (err) => {
    if (err.response.status === 409) {
      setIsLoginExist(true);
      setIsLoading(false);
      setCredential({
        login: '',
      })
    }
  };

  const handleServerError = err => {
    if (err.response.status === 500) {
      setIsLoading(false);
      setServerError(true);
    }
  };

  const handleApiError = (err) => {
    setIsLoading(false);
    setApiError(true);
    console.log(err.response)
  };

  const handleCatch = (err) => {
    if (statusExist(err)) {
      handleExistUser(err)
      handleServerError(err)
    } else {
      handleApiError(err)
    }
  };

  const resetPasswords = () => {
    setCredential({
      password: '',
      repPass: '',
    })
  };

  const resetIsEmptyInputs = () => {
    setNotEmptyInputs({
      login: true,
      email: true,
      password: true,
      repPass: true,
      firstName: true,
      lastName: true,
    })
  };

  const handleOnSubmit = (e) => {
    e.preventDefault();
    resetState();
    resetIsEmptyInputs();

    const {password, repPass, firstName, lastName} = credential;
    if (password === repPass && !isEmptyInputs) {
      registerRequest(credential)
        .then(() => {
          resetState();
          history.push('/');
          context.setRegisterSuccess(true);
          context.setUser(`${firstName} ${lastName}`)
        })
        .catch(err => {
          handleCatch(err)
        })
    } else if (isEmptyInputs) {
      setIsLoading(false);
      setNotEmptyInputs({
        ...credential
      })
    } else {
      setIsDifferentPass(true);
      setIsLoading(false);
      resetPasswords();
    }
  };

  const themeData = {
    isLoading,
    isApiError,
    loginExist,
    isServerError,
    onSubmit: handleOnSubmit,
  };

  const validData = {
    loginExist,
    differentPass,
    notEmptyInputs,
  };

  const registerData = {
    credential,
    themeData,
    validData,
    onChange: handleChange,
  };
  return (
    <RegisterInput {...registerData}/>
  );
};

export default Register;
