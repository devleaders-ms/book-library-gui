import React from 'react';
import PropTypes from 'prop-types'
import Input from '../common/Input';
import LoginRegisterTheme from '../theme/LoginRegisterTheme';
import PopUpAlert from '../popUpAlert/PopUpAlert';

const RegisterInput = ({credential, themeData, validData, onChange}) => {

  return (
    <>
      <LoginRegisterTheme
        secondary
        headerWindow='Zarejestruj się w'
        buttonName='Zarejestruj'
        {...themeData}
      >
        <Input
          id='login'
          autoComplete="off"
          value={credential.login}
          type='text'
          placeholder='login'
          unauthorized={validData.loginExist}
          onChange={onChange}
          isNotEmptyInput={validData.notEmptyInputs.login}
        />
        <Input
          id='email'
          autoComplete="off"
          value={credential.email}
          type='email'
          placeholder='email'
          onChange={onChange}
          isNotEmptyInput={validData.notEmptyInputs.email}
        />
        <Input
          id='firstName'
          autoComplete="off"
          value={credential.firstName}
          type='text'
          placeholder='imię'
          onChange={onChange}
          isNotEmptyInput={validData.notEmptyInputs.firstName}
        />
        <Input
          id='lastName'
          autoComplete="off"
          value={credential.lastName}
          type='text'
          placeholder='nazwisko'
          onChange={onChange}
          isNotEmptyInput={validData.notEmptyInputs.lastName}
        />
        <Input
          id='password'
          autoComplete="off"
          value={credential.password}
          type='password'
          placeholder='hasło'
          onChange={onChange}
          differentPass={validData.differentPass}
          isNotEmptyInput={validData.notEmptyInputs.password}
        />
        <Input
          id='repPass'
          autoComplete="off"
          value={credential.repPass}
          type='password'
          placeholder='powtórz hasło'
          unauthorized={validData.differentPass}
          onChange={onChange}
          isNotEmptyInput={validData.notEmptyInputs.repPass}
        />
      </LoginRegisterTheme>
    </>
  );
};

RegisterInput.propTypes = {
  login: PropTypes.string,
  email: PropTypes.string,
  password: PropTypes.string,
  repPass: PropTypes.string,
  firstName: PropTypes.string,
  lastName: PropTypes.string,
  onChange: PropTypes.func,
  onSubmit: PropTypes.func,
  differentPass: PropTypes.bool,
  loginExist: PropTypes.bool,
  isLoading: PropTypes.bool
};

RegisterInput.defaultProps = {
  login: '',
  email: '',
  password: '',
  repPass: '',
  firstName: '',
  lastName: '',
  differentPass: false,
  loginExist: false,
  isLoading: false,
}

export default RegisterInput;
