import React from 'react';
import {Route} from 'react-router-dom';
import UserPage from '../userPage/UserPage';
import {ManageUsers} from '../manageUsers/ManageUsers';

export const AdminRoutes = () => {
  return (
    <>
      <Route path='/my-profile' component={UserPage}/>
      <Route path='/manage-users' component={ManageUsers}/>
    </>
  )
}
