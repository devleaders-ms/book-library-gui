import React from 'react';
import ROLES from '../Roles';
import {AdminRoutes} from './AdminRoutes';
import {LibrarianRoutes} from './LibrarianRoutes';
import {UserRoutes} from './UserRoutes';

export const AuthenticatedRoutes = ({highestRole}) => {
    switch (highestRole) {
      case ROLES.ADMIN:
        return <AdminRoutes/>
      case ROLES.LIBRARIAN:
        return <LibrarianRoutes/>
      case ROLES.USER:
        return <UserRoutes/>
      default:
        return <></>
    }
}
