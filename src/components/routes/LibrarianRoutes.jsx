import React from 'react';
import {Route} from 'react-router-dom';
import UserPage from '../userPage/UserPage';
import ReservationManage from '../librarianView/ReservationManage';

export const LibrarianRoutes = () => {
  return (
    <>
      <Route path='/my-profile' component={UserPage}/>
      <Route path='/manageReservations' component={ReservationManage}/>
    </>
  )
};
