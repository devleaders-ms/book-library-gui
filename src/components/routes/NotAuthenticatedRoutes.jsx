import {Route} from 'react-router-dom';
import React from 'react';
import Login from '../login/Login';
import Register from '../register/Register';

export const NotAuthenticatedRoutes = () => {
  return (
    <>
      <Route path='/login' component={Login}/>
      <Route path='/register' component={Register}/>
    </>
  )
}
