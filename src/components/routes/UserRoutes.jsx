import {Route} from 'react-router-dom';
import React from 'react';
import UserPage from '../userPage/UserPage';
import MyRentals from '../myRentals/MyRentals';
import MyReservations from '../myReservations/MyReservations';

export const UserRoutes = () => {
  return (
    <>
      <Route path='/my-profile' component={UserPage}/>
      <Route path='/my-rentals' component={MyRentals}/>
      <Route path='/my-reservation' component={MyReservations}/>
    </>
  )
}
