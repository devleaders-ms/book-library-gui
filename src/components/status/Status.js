export const STATUS = {
  AWAITING: 'awaiting',
  RENTED: 'rental',
  RESERVED: 'reserved',
  RETURNED: 'returned',
};
