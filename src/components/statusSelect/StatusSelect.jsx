import React from 'react';
import {STATUS} from '../status/Status';
import Select from 'react-select';
import handleStatus from './handleStatus';
import Button from 'react-bootstrap/Button';

const StatusSelect = ({checked}) => {
  const status = [
    {
      value: STATUS.AWAITING,
      label: 'Oczekujący',
    },
    {
      value: STATUS.RENTED,
      label: 'Wypożyczony'
    },
    {
      value: STATUS.RESERVED,
      label: 'Zarezerwowany'
    },
    {
      value: STATUS.RETURNED,
      label: 'Zwrócony'
    },
  ];

  const defaultOption = status[0];

  const {handleChangeStatus, handleSubmitStatus, selectedStatus} = handleStatus(checked, defaultOption);

  return (
    <div className='d-flex flex-column align-items-end mt-4'>
      <Select
        className='w-25 '
        options={status}
        onChange={handleChangeStatus}
        placeholder='Wybierz status'
        defaultValue={defaultOption}
      />
      <Button
        className='mt-2'
        onClick={() => handleSubmitStatus(selectedStatus)}
      >
        Zmień Status
      </Button>
    </div>
  );
};

StatusSelect.propTypes = {};

export default StatusSelect;
