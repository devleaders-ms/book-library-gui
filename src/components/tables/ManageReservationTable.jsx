import React, {useState} from 'react';
import {UserSelect} from '../manageUsers/UsersSelect';
import universalTable from '../Table/universalTable';
import StatusSelect from '../statusSelect/StatusSelect';
import PropTypes from 'prop-types';

const ManageReservationTable = ({headersList, bodyData}) => {

  const [filteredUser, setFilteredUser] = useState({});

  const {table, checked} = universalTable(headersList, bodyData, filteredUser);

  const handleChange = (selected) => {
    setFilteredUser(selected)
  };

  return (
    <>
      <UserSelect
        selectTitle='Wyszukaj użytkownika'
        handleChange={handleChange}
      />
      {table()}
      <StatusSelect checked={checked}/>
    </>
  );
};

ManageReservationTable.propTypes = {
  headersList: PropTypes.arrayOf(PropTypes.string),
  bodyData: PropTypes.array,
};

export default ManageReservationTable;
