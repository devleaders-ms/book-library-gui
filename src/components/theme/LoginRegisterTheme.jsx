import React from 'react';
import {LogoImage} from '../common/LogoImage';
import Button from 'react-bootstrap/Button';
import styled, {css} from 'styled-components';
import {rgba} from 'polished';
import Form from 'react-bootstrap/Form';
import PropTypes from 'prop-types';
import LoadingSpinner from '../loadingSpinner/LoadingSpinner';
import PopUpAlert from '../popUpAlert/PopUpAlert';
import ErrorAuthenticationMessage from '../errors/ErrorAuthenticationMessage';

const logoIcon = require('../../images/logo.png');

const StyledWrapper = styled.div.attrs({
  className: 'd-flex flex-column align-items-center justify-content-center rounded-lg ml-auto mr-auto'
})`
  position: relative;
  width: 350px;
  height: 300px;
  border: 2px solid #ddd;
  box-shadow: 0px 20px    40px -5px ${rgba('grey', 0.3)};
  ${({secondary}) => (
  secondary && css`
      height: 450px;
   `)}
`;

const StyledForm = styled(Form).attrs({
  className: 'd-flex flex-column align-items-end'
})`
`;

const StyledRegisterTitle = styled.div.attrs({
  className: 'd-flex flex-row align-items-center mb-3'
})`
  font-size: 24px;
`;

const LoginRegisterTheme = (
  {onSubmit, buttonName, headerWindow, secondary, isLoading, isApiError, loginExist, isServerError, unauthorized, children}
) => (
  <>
    <StyledWrapper secondary={secondary}>
      {isLoading && <LoadingSpinner/>}
      <StyledRegisterTitle>
        {headerWindow}
        <LogoImage src={logoIcon}/>
      </StyledRegisterTitle>
      {isApiError && (
        <PopUpAlert popup secondary>
          Ups! Coś poszło nie tak, spróbuj ponownie za chwilę lub skontaktuj się
          z administratorem.
        </PopUpAlert>
      )}
      {isServerError && (
        <PopUpAlert popup secondary>
          Server nie odpowiada, spróbuj ponownie za chilę.
        </PopUpAlert>
      )}
      <StyledForm onSubmit={onSubmit}>
        <>
          {children}
          {unauthorized && (
            <ErrorAuthenticationMessage secondary>
              Niestety nie możemy Cię znaleźć :(
            </ErrorAuthenticationMessage>
          )}
          {loginExist && (
            <ErrorAuthenticationMessage secondary>
              Wybrany login już istnieje.
            </ErrorAuthenticationMessage>
          )}
          <Button variant='outline-primary' type='submit'
                  className='w-50 mt-4'>
            {buttonName}
          </Button>
        </>
      </StyledForm>
    </StyledWrapper>
  </>
);

LoginRegisterTheme.propTypes = {
  onSubmit: PropTypes.func,
  buttonName: PropTypes.string.isRequired,
  headerWindow: PropTypes.string.isRequired,
  secondary: PropTypes.bool,
  isLoading: PropTypes.bool,
  unauthorized: PropTypes.bool,
};

LoginRegisterTheme.defaultProps = {
  secondary: false,
  isLoading: false,
  unauthorized: false,
};

export default LoginRegisterTheme;
