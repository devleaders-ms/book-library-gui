import React from 'react';
import LoadingSpinner from '../loadingSpinner/LoadingSpinner';
import Error500 from '../errors/Error500';
import Error404 from '../errors/Error404';
import UnknownApiError from '../errors/UnknownApiError';
import loadBooks from '../../api/loadBooks';
import LackBooks from '../errors/LackBooks';

const getBooksTheme = (url, headers) => {

  const {errors, resources} = loadBooks(url, headers);
  const {isLoading, isServerError, isApiError, isNotFound, isEmptyData} = errors;

  const handleErrors = () => (
    <>
      {isLoading && <LoadingSpinner center/>}
      {isServerError && <Error500/>}
      {isApiError && <UnknownApiError/>}
      {isNotFound && <Error404/>}
    </>
  );

  return ({handleErrors, resources, isEmptyData});
};

export default getBooksTheme;
