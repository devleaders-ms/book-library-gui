import React from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';
import LackOfBooksMessage from '../errors/LackOfBooksMessage';
import getBooksTheme from './getBooksTheme';
import {getAuthHeaders} from '../../api/apiUtils';

const StyledHead = styled.div.attrs({
  className: 'd-flex justify-content-center ml-0 mb-5 p-2'
})`
  background-color: darkolivegreen;
  color: white;
  font-size: 20px;
`;

const rentalsReservationTheme = (url) => {
  const {handleErrors, resources, isEmptyData} = getBooksTheme(url, getAuthHeaders);

  const rrTheme = (titleHead, libraryOption) => (
    <>
      <StyledHead>{titleHead}</StyledHead>
      {handleErrors()}
      {isEmptyData && <LackOfBooksMessage libraryOption={libraryOption}/>}
    </>
  );

  return ({rrTheme, resources});
};

export default rentalsReservationTheme;
