import React from 'react';
import PropTypes from 'prop-types';
import Form from 'react-bootstrap/Form';
import Button from 'react-bootstrap/Button';
import Input from '../common/Input';

const SetCredentialForm = ({value, repValue, placeholderFirst, placeholderSecond, onSubmit, notEmptyInputs, differentValues, differentPassword, ...props}) => {

  return(
  <Form className='d-flex flex-column align-items-end' onSubmit={onSubmit}>
    <Input
      id='value'
      autoComplete="off"
      value={value}
      placeholder={placeholderFirst}
      isNotEmptyInput={notEmptyInputs.value}
      differentPass={differentPassword}
      differentValues={differentValues}
      {...props}
    />
    <Input
      id='repValue'
      autoComplete="off"
      value={repValue}
      placeholder={placeholderSecond}
      isNotEmptyInput={notEmptyInputs.repValue}
      unauthorized={differentValues || differentPassword}
      {...props}
    />
    <Button size='sm' variant='outline-primary' type='submit' className='w-50 mt-4 mb-1'>
      Aktualizuj
    </Button>
  </Form>
)};

SetCredentialForm.propTypes = {
  value: PropTypes.string,
  repValue: PropTypes.string,
  placeholderFirst: PropTypes.string,
  placeholderSecond: PropTypes.string,
  onSubmit: PropTypes.func,
  onChange: PropTypes.func,
};

export default SetCredentialForm;
