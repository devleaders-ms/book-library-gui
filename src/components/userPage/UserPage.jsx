import React, {useState} from 'react';
import styled from 'styled-components';
import Button from 'react-bootstrap/Button';
import UserPageSetCredential from './UserPageSetCredential';
import {logout} from '../../api/authService';
import {StyledWrapper} from './wrapper/StyledWrapper';

const StyledUserInfoWrapper = styled.div.attrs({
  className: 'd-flex flex-column align-items-center justify-content-start'
})``;

const StyledAvatar = styled.div`
  width: 85px;
  height: 85px;
  border-radius: 50%;
  background-color:darkgreen;
  color: white;
  font-size: 40px;
  text-align:center;
  line-height: 85px;
`;

const StyledUserData = styled.div`
  font-size: 14px;
  font-weight: bolder;
`

const UserPage = () => {

  const [isOpenUserServiceWindow, setIsOpenUserServiceWindow] = useState(false);
  const [isOpenUserPage, setIsOpenUserPage] = useState(true);

  const handleToggleUserServiceWindow = () => {
    setIsOpenUserServiceWindow(!isOpenUserServiceWindow);
    setIsOpenUserPage(!isOpenUserPage);
  };

  const getFirstCapitalLetter = () => (
    localStorage.getItem('user_name').charAt(0).toUpperCase()
  );

  return (
    <>
      {isOpenUserPage &&
      <div className='d-flex justify-content-center'>
        <StyledWrapper>
          <StyledUserInfoWrapper>
            <StyledAvatar>{getFirstCapitalLetter()}</StyledAvatar>
            <StyledUserData className='mt-3 font-weight-bold'>
              {localStorage.getItem('user_name')}
            </StyledUserData>
            <StyledUserData className='font-weight-normal'>
              {localStorage.getItem('user_email')}
            </StyledUserData>
            <Button size='sm' className='mt-3' onClick={handleToggleUserServiceWindow}>
              Zarządzaj kontem
            </Button>
          </StyledUserInfoWrapper>
          <Button variant='light' onClick={() => logout()}>
            Wyloguj
          </Button>
        </StyledWrapper>
      </div>}
        {isOpenUserServiceWindow &&
        <UserPageSetCredential toggleServiceWindow={handleToggleUserServiceWindow}/>}
    </>
  )
};

export default UserPage;
