import React, {useRef, useState} from 'react';
import PropTypes from 'prop-types'
import styled from 'styled-components';
import Button from 'react-bootstrap/Button';
import Nav from 'react-bootstrap/Nav';
import UpdateData from './changeCredentials/UpdateData';
import SetCredentialForm from './SetCredentialForm';
import {StyledWrapper} from './wrapper/StyledWrapper';
import {closeByClickOutside} from '../closeByClickOutside/closeByClickOutside';
import LoadingSpinner from '../loadingSpinner/LoadingSpinner';
import PopUpAlert from '../popUpAlert/PopUpAlert';

const StyleContent = styled.div.attrs({
  className: 'd-flex align-items-center justify-content-center rounded-bottom'
})`
  width: 241px;
  height: 270px;
  border: 1px solid #ddd;
  border-top: none;
`;

const StyledNav = styled(Nav).attrs({
  variant: 'tabs',
  defaultActiveKey: 'link-0',
  className: 'mt-3'
})``;

const StyledNavLink = styled(Nav.Link).attrs({
  className: 'text-secondary'
})``;

const UserPageSetCredential = ({toggleServiceWindow}) => {

  const [isPassword, setIsPassword] = useState(true);
  const userPageSetCredentialRef = useRef(null);

  const {setCredentialData, ...data} = UpdateData(isPassword);

  const clearInput = (clear) => {
    clear.clearState();
    clear.resetIsEmptyInputs();
    clear.setIsDifferentValues(false);
    clear.setIsDifferentPassword(false);
  };

  const toggleEmail = (clear) => {
    setIsPassword(false);
    clearInput(clear)
  };

  const togglePassword = (clear) => {
    setIsPassword(true);
    clearInput(clear);
  };

  closeByClickOutside(toggleServiceWindow, userPageSetCredentialRef);

  return (
    <>
      <div className='d-flex justify-content-center align-items-center'>
        <StyledWrapper secondary ref={userPageSetCredentialRef}>
          <div className='align-self-end'>
            <Button variant='outline-danger'
                    size='sm'
                    className='mr-3 mt-1'
                    onClick={toggleServiceWindow}>
              X
            </Button>
          </div>
          <>
            <StyledNav fill>
              <Nav.Item>
                <StyledNavLink
                  onSelect={() => togglePassword(setCredentialData)}
                  eventKey="link-0"
                >
                  Zmień hasło
                </StyledNavLink>
              </Nav.Item>
              <Nav.Item>
                <StyledNavLink
                  onSelect={() => toggleEmail(setCredentialData)}
                  eventKey="link-1"
                >
                  Zmień email
                </StyledNavLink>
              </Nav.Item>
            </StyledNav>

            <StyleContent>
              {isPassword ? (
                <SetCredentialForm
                  placeholderFirst='nowe hasło'
                  placeholderSecond='powtórz hasło'
                  type='password'
                  {...data}
                />
              ) : (
                <SetCredentialForm
                  placeholderFirst='nowy email'
                  placeholderSecond='powtórz email'
                  {...data}
                />
              )}
            </StyleContent>
          </>
        </StyledWrapper>
        {setCredentialData.isLoading && <LoadingSpinner/>}
      </div>
      {setCredentialData.isApiError && (
        <PopUpAlert popup secondary ms={4000}>
          Ups! Coś poszło nie tak, spróbuj ponownie za chwilę lub skontaktuj się
          z administratorem.
        </PopUpAlert>)}
      {setCredentialData.isServerError && (
        <PopUpAlert popup secondary>
          Server nie odpowiada, spróbuj ponownie za chilę.
        </PopUpAlert>)}
    </>
  );
};

UserPageSetCredential.propTypes = {
  value: PropTypes.string,
  repValue: PropTypes.string,
  onChange: PropTypes.func,
  onSubmit: PropTypes.func,
  onClick: PropTypes.func,
  onSelect: PropTypes.func
};

export default UserPageSetCredential;
