import React, {useContext, useEffect, useReducer, useState} from 'react';
import {statusExist, updateDataRequest} from '../../../api/authService';
import {AuthenticationContext} from '../../context/context';
import {findEmptyInput} from '../../../api/apiUtils';
import { useHistory } from 'react-router';

const UpdateData = (isPassword) => {

  const [values, setValue] = useReducer((prev, state) => ({...prev, ...state}),
    {
      value: '',
      repValue: '',
    });

  const [notEmptyInputs, setNotEmptyInputs] = useReducer((prev, state) => ({...prev, ...state}),
    {
      value: true,
      repValue: true,
    });

  const [isLoading, setIsLoading] = useState(false);
  const [differentValues, setIsDifferentValues] = useState(false);
  const [differentPassword, setIsDifferentPassword] = useState(false);
  const [isApiError, setApiError] = useState(false);
  const [isServerError, setServerError] = useState(false);

  const isEmptyInputs = findEmptyInput(values);
  const context = useContext(AuthenticationContext);
  const history = useHistory();

  const handleOnChange = (e) => {
    const {value, id} = e.target;
    setValue({
      [id]: value,
    });
  };

  const clearState = () => {
    setValue({
      value: '',
      repValue: '',
    })
  };

  useEffect(() => {
    setTimeout(() => {
      setServerError(false);
      setApiError(false);
    }, 8000)
  }, [isServerError, isApiError]);

  const handleServerError = err => {
    if (err.response.status === 500) {
      setIsLoading(false);
      setServerError(true);
    }
  };

  const handleApiError = (err) => {
    setIsLoading(false);
    setApiError(true);
  };

  const handleCatch = (err) => {
    if (statusExist(err)) {
      handleServerError(err)
    } else {
      handleApiError(err)
    }
  };

  const resetState = () => {
    setIsDifferentValues(false);
    setIsDifferentPassword(false);
    setIsLoading(true);
    setApiError(false);
  };

  const resetIsEmptyInputs = () => {
    setNotEmptyInputs({
      value: true,
      repValue: true,
    })
  };

  const setCorrectObjName = value => {
    return isPassword ? {password: value} : {email: value};
  };

  const setEmailInLocalStorage = (email) => {
    if (!isPassword) {
      localStorage.setItem('user_email', email);
    }
  };

  const handleOnSubmit = (e) => {
    e.preventDefault();
    resetState();
    resetIsEmptyInputs();

    const {value, repValue} = values;

    if (value === repValue && !isEmptyInputs) {
      updateDataRequest(setCorrectObjName(value))
        .then(() => setEmailInLocalStorage(value))
        .then(() => {
          context.setIsUpdateSuccess(true)
          history.push('/');
        })
        .catch(err => handleCatch(err));
      clearState()
    } else if (isEmptyInputs) {
      setIsLoading(false);
      setNotEmptyInputs({
        ...values
      })
    } else if (value !== repValue && isPassword) {
      setIsLoading(false);
      setIsDifferentPassword(true);
    } else {
      setIsLoading(false);
      setIsDifferentValues(true)
    }
  };

  const setCredentialData = {
    clearState,
    resetIsEmptyInputs,
    isLoading,
    setIsDifferentValues,
    setIsDifferentPassword,
    isApiError,
    isServerError,
  };

  const renderProps = {
    ...values,
    notEmptyInputs,
    differentValues,
    differentPassword,
    setCredentialData,
    onChange: handleOnChange,
    onSubmit: handleOnSubmit,
  };
  return (renderProps);
};

export default UpdateData;
