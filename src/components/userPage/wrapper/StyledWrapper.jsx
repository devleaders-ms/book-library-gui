import styled, {css} from 'styled-components';
import {rgba} from 'polished';

export const StyledWrapper = styled.div.attrs({
  className: 'd-flex flex-column align-items-center justify-content-between mt-3 w-25 pt-3 pb-5 rounded'
})`
  position: relative;
  height: 400px;
  border: 2px solid #ddd;
  box-shadow: 0px 20px 40px -5px ${rgba('grey', 0.3)};
  ${({secondary}) => (
  secondary && css`
    height: 300px;
  `)}
`;
