export const exampleRental = [
  {
    id: 1,
    bookName: 'harry potter i książe półkrwi  drużyna pierścienia',
    copyNumber: 1,
    rentalData: '12.04.2019',
    returnData: '12.04.2019',
    status: 'wypożyczony',
    img: 'src/images/potter.jpg'
  },
  {
    id: 2,
    bookName: 'harry potterharry potter i książe półkrwi  drużyna pierścienia',
    copyNumber: 1,
    rentalData: '12.04.2019',
    returnData: '12.04.2019',
    status: 'wypożyczony',
    img: 'src/images/potter.jpg'
  },
  {
    id: 3,
    bookName: 'harry potterharry potter i książe półkrwi  drużyna pierścienia',
    copyNumber: 1,
    rentalData: '12.04.2019',
    returnData: '12.04.2019',
    status: 'wypożyczony',
    img: 'src/images/potter.jpg'
  },
  {
    id: 4,
    bookName: 'harry potterharry potter i książe półkrwi  drużyna pierścienia',
    copyNumber: 1,
    rentalData: '12.04.2019',
    returnData: '12.04.2019',
    status: 'wypożyczony',
    img: 'src/images/potter.jpg'
  },
  {
    id: 5,
    bookName: 'harry potterharry potter i książe półkrwi  drużyna pierścienia',
    copyNumber: 1,
    rentalData: '12.04.2019',
    returnData: '12.04.2019',
    status: 'wypożyczony',
    img: 'src/images/potter.jpg'
  },
  {
    id: 6,
    bookName: 'harry potterharry potter i książe półkrwi  drużyna pierścienia',
    copyNumber: 1,
    rentalData: '12.04.2019',
    returnData: '12.04.2019',
    status: 'wypożyczony',
    img: 'src/images/potter.jpg'
  },
]

export const exampleReservation = [
  {
    id: 1,
    bookName: 'siała baba mak nie wiedziała jak ',
    copyNumber: 4,
    reservationData: '14.02.3045',
    reservationOffData: '23.10.4560',
    img: 'src/images/faustyna.jpg',
  },
  {
    id: 2,
    bookName: 'siała baba mak nie wiedziała jak a dziad wiedział nie powiedziała a to było tak',
    copyNumber: 4,
    reservationData: '14.02.3045',
    reservationOffData: '23.10.4560',
    img: 'src/images/faustyna.jpg',
  },
  {
    id: 3,
    bookName: 'siała baba mak nie wiedziała jak a dziad wiedział nie powiedziała a to było tak',
    copyNumber: 4,
    reservationData: '14.02.3045',
    reservationOffData: '23.10.4560',
    img: 'src/images/faustyna.jpg',
  },
  {
    id: 4,
    bookName: 'siała baba mak nie wiedziała jak a dziad wiedział nie powiedziała a to było tak',
    copyNumber: 4,
    reservationData: '14.02.3045',
    reservationOffData: '23.10.4560',
    img: 'src/images/faustyna.jpg',
  },
  {
    id: 5,
    bookName: 'siała baba mak nie wiedziała jak a dziad wiedział nie powiedziała a to było tak',
    copyNumber: 4,
    reservationData: '14.02.3045',
    reservationOffData: '23.10.4560',
    img: 'src/images/faustyna.jpg',
  },
  {
    id: 6,
    bookName: 'siała baba mak nie wiedziała jak a dziad wiedział nie powiedziała a to było tak',
    copyNumber: 4,
    reservationData: '14.02.3045',
    reservationOffData: '23.10.4560',
    img: 'src/images/faustyna.jpg',
  },
];

export const tableHead = ['Id', 'Id książki', 'Użytkownik', 'Data rezerwacji', 'Status', '#'];
export const tableData = [
  {
    id: 0,
    bookItemId: 5,
    userId: 'adminName',
    reservationDate: "2020-05-20",
    status: "AWAITING"
  },
  {
    id: 1,
    bookItemId: 12,
    userId: 'adminName',
    reservationDate: "2020-05-20",
    status: "AWAITING"
  },
  {
    id: 2,
    bookItemId: 32,
    userId: 'userName',
    reservationDate: "2020-05-20",
    status: "AWAITING"
  },
  {
    id: 3,
    bookItemId: 4,
    userId: 'userName',
    reservationDate: "2020-05-20",
    status: "AWAITING"
  },
  {
    id: 4,
    bookItemId: 9,
    userId: 'adminName',
    reservationDate: "2020-05-20",
    status: "AWAITING"
  },
];
